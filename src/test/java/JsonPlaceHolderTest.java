import config.TestConfig;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static constants.Constants.Actions.*;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.requestSpecification;

public class JsonPlaceHolderTest extends TestConfig {


    @Test
    public void GetPlaces () {
        given().queryParam("postId", 1).log().uri().
                when().get(JSON_PLACE_HOLDER_GET).
                then().log().body().statusCode(200);
    }

    @Disabled
    @Test
    public  void Put () {
        String putBodyJson = "{\n" +
                "\"id\":1,\n" +
                "\"title\":\"foo\",\n" +
                "\"body\":\"bar\",\n" +
                "\"userId\":1\n" +
                "}";

        given().body(putBodyJson).log().uri().
                when().put(JSON_PLACE_HOLDER_PUT).
                then().log().body().statusCode(200);
    }

    @Disabled
    @Test
    public void Delete() {
        given().log().uri().
                when().delete(JSON_PLACE_HOLDER_DELETE).
                then().log().body().statusCode(200);
    }


    @Test
    public void PostWithJson () {
        String postBodyJson = "{\n" +
                "\"title\":\"foo\",\n" +
                "\"body\":\"bar\",\n" +
                "\"userId\":1\n" +
                "}";

        given().body(postBodyJson).log().all().
                when().post(JSON_PLACE_HOLDER_POST).
                then().spec(responseSpecificationForPost).log().body();
    }

    @Test
    public void PostWithXml () {
        String postXmlBody = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n" +
                "<employees>\n" +
                "\t<employee>\n" +
                "\t\t<id>1</id>\n" +
                "\t\t<firstName>Tom</firstName>\n" +
                "\t\t<lastName>Cruise</lastName>\n" +
                "\t\t<photo>https://jsonformatter.org/img/tom-cruise.jpg</photo>\n" +
                "\t</employee>\n" +
                "\t<employee>\n" +
                "\t\t<id>2</id>\n" +
                "\t\t<firstName>Maria</firstName>\n" +
                "\t\t<lastName>Sharapova</lastName>\n" +
                "\t\t<photo>https://jsonformatter.org/img/Maria-Sharapova.jpg</photo>\n" +
                "\t</employee>\n" +
                "\t<employee>\n" +
                "\t\t<id>3</id>\n" +
                "\t\t<firstName>Robert</firstName>\n" +
                "\t\t<lastName>Downey Jr.</lastName>\n" +
                "\t\t<photo>https://jsonformatter.org/img/Robert-Downey-Jr.jpg</photo>\n" +
                "\t</employee>\n" +
                "</employees>";

        given().spec(requestSpecificationXml).body(postXmlBody).log().all().
                when().post("").
                then().log().body().statusCode(200);
    }
}
