import config.TestConfig;

import io.restassured.response.Response;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static constants.Constants.Actions.SWAPI_GET_PEOPLE;
import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.equalTo;

public class FirstTest extends TestConfig{


    @Test
    public void myFirstTest() {
        given().spec(requestSpecificationForGet).log().uri().
                when().get(SWAPI_GET_PEOPLE +"1").
                then().log().body().statusCode(200);
    }

    @Test
    public void getSomeFieldInResponseAssertion () {
        given().spec(requestSpecificationForSwapi).log().all().
                when().get("").
                then().body("people", equalTo("https://swapi.dev/api/people/")).log().body().statusCode(200);
    }

    @Test
    public void getSomeFieldInResponseAssertionWithIndex () {
        given().spec(requestSpecificationForSwapi).log().all().
                when().get(SWAPI_GET_PEOPLE).
                then().body("count", equalTo(82)).
                body("results.name[0]",equalTo("Luke Skywalker")).
                log().body().statusCode(200);
    }

    @Test
    public void getAllDataFromRequest () {
        Response response =
                given().spec(requestSpecificationForSwapi).log().all().
                when().get("").
                then().extract().response();

        String jsonResponseAsString = response.asString();
        System.out.println(jsonResponseAsString);
    }

    @Test
    public void getCookieFromResponse () {
        Response response =
                given().spec(requestSpecificationForSwapi).log().all().
                        when().get("").
                        then().extract().response();
        Map<String, String> allCookie = response.getCookies();
    }
}
