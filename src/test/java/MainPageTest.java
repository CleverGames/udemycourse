import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class MainPageTest {
    private WebDriver driver;
    private MainPage mainPage;

    @BeforeEach
    public void setUp(){
        open("http://github.com");
        driver = getWebDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        mainPage = PageFactory.initElements(driver, MainPage.class);
    }

    @Test
    public void signInTest(){
        LoginPage loginPage = mainPage.clickSignIn();
        String heading = loginPage.getHeadingText();

        Assertions.assertEquals("Sign in to GitHub", heading);
    }

    @Test
    public void registerPassTest() {
        SignUpPage signUpPage = mainPage.register("testsets@mail.ru");
        WebDriverWait wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@data-continue-to='password-container']")));
        String welcomeText = signUpPage.getHeadingText();
        Assertions.assertEquals("Welcome to GitHub!\n" +
                "Let’s begin the adventure", welcomeText);
    }

    @Disabled
    @Test
    public void registerFailTest(){
        SignUpPage signUpPage = mainPage.register("testuser");
        String error = signUpPage.getMainErrorText();
        Assertions.assertEquals("There were problems creating your account.", error);
    }

    @Disabled
    @Test
    public void signUpEmptyUsernameTest(){
        SignUpPage signUpPage = mainPage.register("");
        String error = signUpPage.getUsernameErrorText();
        Assertions.assertEquals("Login can't be blank", error);
    }

    @Disabled
    @Test
    public void signUpInvalidEmailTest(){
        SignUpPage signUpPage = mainPage.register("qeqwe");
        String error = signUpPage.getEmailErrorText();
        Assertions.assertEquals("Email is invalid or already taken", error);
    }

    @AfterEach
    public void tearDown(){
        driver.quit();
    }
}
