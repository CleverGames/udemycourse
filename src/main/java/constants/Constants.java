package constants;

import static constants.Constants.Path.JSON_PLACE_HOLDER_PATH;
import static constants.Constants.Path.SWAPI_PATH;
import static constants.Constants.Servers.JSON_PLACE_HOLDER_URL;
import static constants.Constants.Servers.SWAPI_URL;

public class Constants {

    public static class RunVariable{
        public static String server = JSON_PLACE_HOLDER_URL;
        public static String path = JSON_PLACE_HOLDER_PATH;
    }

    public static class Servers {
        public static String SWAPI_URL = "https://swapi.dev/";
        public static String JSON_PLACE_HOLDER_URL = "https://jsonplaceholder.typicode.com/";
        public static String REQUEST_BIN_URL = "https://encu54ywsnpgqmr.m.pipedream.net";
        public static String GOOGLE_PLACES_URL;
    }

    public static class Path {
        public static String SWAPI_PATH = "api/";
        public static String JSON_PLACE_HOLDER_PATH = "";
        public static String REQUEST_BIN_PATH = "";
        public static String GOOGLE_PLACES_PATH;
    }

    public static class Actions {
        public static String SWAPI_GET_PEOPLE = "people/";
        public static String GOOGLE_PLACES_PATH;
        public static String JSON_PLACE_HOLDER_GET = "/comments";
        public static String JSON_PLACE_HOLDER_PUT = "posts/1";
        public static String JSON_PLACE_HOLDER_DELETE = "posts/1";
        public static String JSON_PLACE_HOLDER_POST = "posts";
    }


}
