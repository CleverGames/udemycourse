import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage {

    private WebDriver driver;

    public MainPage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(xpath = "//a[@href = '/login' and @class = 'HeaderMenu-link flex-shrink-0 no-underline']")
    private WebElement signInButton;

    @FindBy(xpath = "//a[@href = '/signup?ref_cta=Sign+up&ref_loc=header+logged+out&ref_page=%2F&source=header-home']/parent::div[@class = 'd-flex flex-items-center']/a")
    private WebElement signUpButton;
  /*  @FindBy(xpath = ".//*[@id='user[login]']")
    private WebElement userNameField; */
    @FindBy(xpath = "//input[@placeholder = 'Email address']")
    private WebElement emailField;
 /*   @FindBy(xpath = ".//*[@id='user[password]']")
    private WebElement passwordField; */
    @FindBy(xpath = "//button[@class = 'btn-mktg btn-primary-mktg width-full width-sm-auto']")
    private WebElement signUpFormButton;

    public LoginPage clickSignIn(){
        signInButton.click();
        return new LoginPage(driver);
    }

    public SignUpPage clickSignUpButton(){
        signUpButton.click();
        return new SignUpPage(driver);
    }

    public SignUpPage clickSignUpFormButton(){
        signUpFormButton.click();
        return new SignUpPage(driver);
    }

    /*public MainPage typeUserName(String username){
        userNameField.sendKeys(username);
        return this;
    }*/

    /*public MainPage typePassword(String password){
        passwordField.sendKeys(password);
        return this;
    }*/

    public MainPage typeEmail(String email){
        emailField.sendKeys(email);
        return this;
    }

    public SignUpPage register(String email){
       // this.typeUserName(username);
        this.typeEmail(email);
       // this.typePassword(password);
        this.clickSignUpFormButton();
        return new SignUpPage(driver);
    }


}

